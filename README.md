# Proyecto FrontEnd Avanzza
## Por José Zevallos Caycho
----------
## Requisitos
    NodeJS Versión -> 14.17.2
    NPM Versión -> 6.14.13
    VueJS Versión -> 3
## Instalación

Clonar el respositorio

    git clone https://gitlab.com/phantomzx03/proyecto_frontend_avanzza.git

Cambiar a la carpeta del repositorio

    cd proyecto_frontend_avanzza

### Instalar dependendcias

    npm install


### Compila y recarga en caliente para el desarrollo

    npm run serve


### Compila y minimiza para producción

    npm run build

**Nota : En la carpeta excel se encuentra el archivo para la subida masiva de ligas con equipos.


### Configuración Personalizada
Ver [Configuración de referencia](https://cli.vuejs.org/config/).

