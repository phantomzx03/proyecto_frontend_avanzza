import axios from "axios";
//Token de acceso Libre
//const access_token = "$2y$10$LVJWOYUYDjzqbvN8L.E2auGQEHxEiRa/mYSiHGWakFE/9N1USansa";
//<==================================================================================>
//Token de acceso de Paga
const access_token =
  "$2y$10$MKJxX2GV/BHCA97Q677rYOUg3q.OVoBkQogeNERs3JrE59LlNfrH6";
//<==================================================================================>
export default axios.create({
  baseURL: `http://127.0.0.1:8000/api/`,
  headers: {
    Authorization: `Bearer ${access_token}`,
  },
});
